const app = require('express')();
const http = require('http').Server(app);
const io = require('socket.io')(http);
const port = process.env.PORT || 3000;
const MongoClient = require('mongodb').MongoClient;
const bodyParser = require('body-parser');
const Promise = require('bluebird');
const config = require('./config');
const error = require('./error');
const jwt = require('jsonwebtoken'); 
const fs = require('fs');
app.set('view engine', 'ejs');
app.use(bodyParser.urlencoded({extended:true}));
//app.use('/getRoom');
let roomId;
app.get('/chat/:roomId', function(req, res) {     // router to GUI test
  if (req.url === '/favicon.ico') {
    res.writeHead(200, {'Content-Type': 'image/x-icon'} );
    res.end(/* icon content here */);
  } else {
    roomId = req.params.roomId.toString();
    res.render(__dirname + '/index', {roomId: req.params.roomId.toString()});
  }
});

// ==== TEST CHAT HARDCODE =====
app.get('/chat_user1/:roomId', function(req, res) {     // router to GUI test
  if (req.url === '/favicon.ico') {
    res.writeHead(200, {'Content-Type': 'image/x-icon'} );
    res.end(/* icon content here */);
  } else {
    roomId = req.params.roomId.toString();
    res.render(__dirname + '/index_user1', {roomId: req.params.roomId.toString()});
  }
});

app.get('/chat_user2/:roomId', function(req, res) {     // router to GUI test
  if (req.url === '/favicon.ico') {
    res.writeHead(200, {'Content-Type': 'image/x-icon'} );
    res.end(/* icon content here */);
  } else {
    roomId = req.params.roomId.toString();
    res.render(__dirname + '/index_user2', {roomId: req.params.roomId.toString()});
  }
});

app.get('/chat_fake/:roomId', function(req, res) {     // router to GUI test
  if (req.url === '/favicon.ico') {
    res.writeHead(200, {'Content-Type': 'image/x-icon'} );
    res.end(/* icon content here */);
  } else {
    roomId = req.params.roomId.toString();
    res.render(__dirname + '/index_fake', {roomId: req.params.roomId.toString()});
  }
});
// ==========================

let notiRoomId;
app.get('/noti/:roomId', function(req, res) {     // router to GUI test
  if (req.url === '/favicon.ico') {
    res.writeHead(200, {'Content-Type': 'image/x-icon'} );
    res.end(/* icon content here */);
  } else {
    notiRoomId = req.params.roomId.toString();
    res.render(__dirname + '/notification', {notiRoomId: req.params.roomId.toString()});
  }
});

//let userId;
app.get('/listRoom', function(req, res){     // router to GUI test
    //userId = req.params.userId.toString();
    res.render(__dirname + '/listRoom');
});

let notifyObj;
app.post('/sendNotify', function (req, res) {
	if (req.url === '/favicon.ico') {
		res.writeHead(200, {'Content-Type': 'image/x-icon'});
		res.end(/* icon content here */);
	} else {

		io.emit('others', {message: 'You has new notification.'})
		res.send(JSON.stringify({success: true, req: req.url}));
	}
});

app.get('/getToken', function(req, res) {
  let token = jwt.sign({foo: 'bar'}, 'shhhhhh');
  res.send(token); 
});

MongoClient.connect(config.MONGO_URI,  {
  reconnectTries: Number.MAX_VALUE,
  reconnectInterval: 1000
  }, function(err, db) {
  if (err) {
    throw err;
  } 
  console.log('Mongodb connected ...');
  let mongo = db.db(config.MONGO_DB);
  
  // khai báo một socket connection
  io.on('connection', function(socket) {
    socket.on('authenticate', function (data) {
      console.log('data: ', data);
      // check data send to client
      let private_key = 'SGF2ZSB0byBkZWFsIHdpdGggQmFzZTY';
      jwt.verify(data.token, private_key, function(err, decoded) {
        if (err) {
          console.log('socketId: ' + socket.id + ' is unauthenticazed');
          socket.emit('authenticateFailure', {err: 'Unauthenticated'});
      
          socket.userID = 0
          socket.objectId = -1
          socket.disconnect();
          return;
        } else {
          console.log('[index.js][connection] --- decoded: ', decoded);
          if (decoded['driver_id'] != undefined) {
            socket.userID = 'driver' + decoded['driver_id'];
            socket.objectId = config.DRIVER;
          } else if (decoded['user_id'] != undefined) {
            socket.userID = 'customer' + decoded['user_id'];
            socket.objectId = config.CUSTOMER;
          } else if (decoded['manager_id'] != undefined) {
            socket.userID = 'dispatch' + decoded['manager_id'];
            socket.objectId = config.DISP;
          } else {
            console.log('user verify failure');
            socket.userID = 0
            socket.objectId = -1
            socket.disconnect();
          }
          console.log('socketId: ' + socket.id + ' token: ' + data.token);
        }
      });
    })

    socket.on('joinRoom', function (msg) {
      console.log('=====> joinRoom: ', msg);
      if (msg.roomID != undefined) {
          socket.join(msg.roomID);
          io.to(msg.roomID).emit('joinRoom', {userID: socket.userID, message: 'user ' + socket.userID + ' joined room ' + msg.roomID});
      }
    });
    //socket.join(roomId);
    
    socket.on(roomId, function(msg) {
      console.log('roomId: ', roomId);
      let notiFlag = roomId.includes(config.NOTI_FLAG);
      console.log('roomId: ', notiFlag);
      if (notiFlag) return;
      mongo.collection(config.MONGO_ROOM_COL).findOne({roomId: roomId}, function (err, res) {
        if (err) throw err;

        if (res && res._id != null) {
          // insert message to exist room
          mongo.collection(config.MONGO_MESSAGE_COL).insertOne({roomId: roomId, from: socket.handshake.query.userId, text: msg.toString(), createdAt: Date.now().toString()}, function (err, res) {
            //console.log('====> res messages: ', res);
            if (err) throw err;
            console.log('Insert user: ' + socket.id + ' chat - ' + msg.toString() + ' - into roomId: ' + roomId + ' at ' + Date.now().toString());
          });
        } else {
          // create rooms when it's not exist
          let roomArr = roomId.split('_');
          mongo.collection(config.MONGO_ROOM_COL).insertOne({roomId: roomId, from: roomArr[0], to: roomArr[1], createdAt: Date.now().toString()}, function (err, res) {
            if (err) throw err;
            mongo.collection(config.MONGO_MESSAGE_COL).insertOne({roomId: roomId, from: socket.handshake.query.userId, text: msg.toString(), createdAt: Date.now().toString()}, function (err, res) {
              //console.log('====> res messages: ', res);
              if (err) throw err;
              console.log('Insert user: ' + socket.id + ' chat - ' + msg.toString() + ' - into roomId: ' + roomId + ' at ' + Date.now().toString());
            });
            console.log('Insert roomId: ' + roomId + ' from: ' + roomArr[0] + ' to: ' + roomArr[1] + ' at ' + Date.now().toString());
          })
        }
      })
      io.to(roomId).emit(roomId, socket.handshake.query.userId + ': ' + msg);
    });

    socket.on(config.GET_LIST_ROOM, function(msg) {
      let fromFlag = '';
      if (socket.objectId === config.DISP) {
        fromFlag = 'from';
      } else if (socket.objectId === config.DRIVER) {
        fromFlag = 'to';
      }

      let obj = new Object();
      obj[fromFlag] = socket.userID;
      
      console.log('[index][GET_LIST_ROOM_DISP] - socket.objectId: ', socket.objectId + ' - socket.userID: ' + socket.userID);
      mongo.collection(config.MONGO_ROOM_COL).find(obj).toArray(function (err, res) {
        if (err) throw err;
        if (res && typeof res[0] != 'undefined') {
          getDetailConversion(res, mongo).then((resultConversion) => {
            console.log('result: ', resultConversion);
            io.sockets.emit(config.GET_LIST_ROOM, resultConversion);
          })
        } else {
          io.sockets.emit(config.GET_LIST_ROOM, {err: error.NOT_ROOM, msg: 'dont have room'});
        }
      })
    })

    socket.on(notiRoomId, function(status) {
      //if (!socket.auth) return;
      // check msg co phai cua notification hay khong
      let notiFlag = notiRoomId.includes(config.NOTI_FLAG);
      console.log('notiRoomId: ', notiFlag);
      if (!notiFlag) return;
      // check status of roomId
      mongo.collection(config.MONGO_NOTI).find({roomId: notiRoomId}).toArray(function (err, res) {
        if (err) throw err;
        if (res && typeof res[0] != 'undefined') {
          
          let flag = false;
          console.log('res: ' + JSON.stringify(res));

          console.log('status: ', JSON)
          for (item in res) {
            if (res[item] == status) {
              console.log('res[item]: ' + res[item] + ' status: ' + status);
              flag = true;
            }
          }
          if (!flag) {
            console.log('flag: ', flag);
            mongo.collection(config.MONGO_NOTI).insertOne({roomId: notiRoomId, from: socket.handshake.query.userId, status: status, createdAt: Date.now().toString()}, function (err, res) {
              if (err) throw err;
              console.log('Insert user: ' + socket.id + ' status - ' + status + ' - into roomId: ' + notiRoomId + ' at ' + Date.now().toString());
              io.to(roomId).emit(notiRoomId, socket.handshake.query.userId + ': ' + status);
            });
          }
        }
        else {
          mongo.collection(config.MONGO_NOTI).insertOne({roomId: notiRoomId, from: socket.handshake.query.userId, status: status, createdAt: Date.now().toString()}, function (err, res) {
            if (err) throw err;
            console.log('Insert user: ' + socket.id + ' status - ' + status + ' - into roomId: ' + notiRoomId + ' at ' + Date.now().toString());
            io.to(roomId).emit(notiRoomId, socket.handshake.query.userId + ': ' + status);
          });
        }
      });   
    });
  });
});

http.listen(port, function() {
  console.log('listening on *:' + port);
});

// ==== FUNCTION =====//
// lay thong tin chi tiet 1 hoi thoai
const getDetailConversion = (params, mongo) => {
  return new Promise((resolve, reject) => {
    Promise.map(params, (item) => {  
      return getDataConversionFromDB(item, mongo);
    }).then(results => {
      resolve(results);
    });
  })
};

const getDataConversionFromDB = (item, mongo) => {
  return new Promise((resolve, reject) => {
        mongo.collection(config.MONGO_MESSAGE_COL).find({roomId: item.roomId.toString()}).toArray(function(err, result) {
          if (err) reject(err);
          let obj = new Object();
          obj['roomId'] = item.roomId.toString();
          obj.message = result;
          resolve(obj);
        })
      })
};
