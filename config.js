exports.MONGO_URI = 'mongodb://localhost:27017/';
exports.MONGO_DB = 'dropChat';
exports.MONGO_MESSAGE_COL = 'messages';
exports.MONGO_ROOM_COL = 'rooms';
exports.GET_LIST_ROOM = 'getListRoom';
exports.MONGO_NOTI = 'notifications';
exports.PUSH_NOTI = 'pushNoti';
exports.NOTI_FLAG = 'noti_';
// status noti
exports.RECEIVED = 'received';
exports.ON_THE_WAY = 'courierIsOnTheWay';
exports.ARRIVED = 'arrived';
exports.DELIVERED = 'delivered';

// disp drive mode at GET_LIST_ROOM
exports.DISP = '0';
exports.DRIVER = '1';
exports.CUSTOMER = '2';